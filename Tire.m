//
//  Tire.m
//  Learning0508
//
//  Created by  apple on 13-5-8.
//  Copyright (c) 2013年 dhy. All rights reserved.
//

#import "Tire.h"

@implementation Tire
//初始化： 轮胎压力和花纹深度均为固定值
-(id)init{
    NSLog(@"父 init");
    if(self = [self initWithPressure:34.0 treadDepth:20.0]){
    }
    return self;
}

//初始化： 指定轮胎压力 ， 花纹深度固定
-(id)initWithPressure:(float) pressureValue{
    NSLog(@"父 initWithPressure");
    if(self = [self initWithPressure:pressureValue treadDepth:20.0]){
        
    }
    return self;
}
//初始化： 指定花纹深度， 轮胎压力固定
-(id)initWithTreadDepth:(float) treadDepthValue{
    NSLog(@"父 initWithTreadDepth");
    if(self = [self initWithPressure:34.0 treadDepth:treadDepthValue]){
        
    }
    return self;
}

//初始化，指定轮胎压力 和 花纹深度
//指定初始化函数
-(id)initWithPressure:(float) pressureValue treadDepth:(float) treadDepthValue{
    NSLog(@"父 initWithPressure:treadDepth");
    if(self = [super init]){
        pressure = pressureValue;
        treadDepth = treadDepthValue;
    }
    return self;
}


-(float)pressure{
    return pressure;
}
-(void)setPressure:(float)pressureValue{
    pressure = pressureValue;
}

-(float)treadDepth{
    return treadDepth;
}
-(void)setTreadDepth:(float)treadDepthValue{
    treadDepth = treadDepthValue;
}

@end
