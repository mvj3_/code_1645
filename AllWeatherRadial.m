//
//  AllWeatherRadial.m
//  Learning0508
//
//  Created by  apple on 13-5-8.
//  Copyright (c) 2013年 dhy. All rights reserved.
//

#import "AllWeatherRadial.h"

@implementation AllWeatherRadial

//初始化，指定轮胎压力 和 花纹深度
//重写父类的指定初始化函数
-(id)initWithPressure:(float) pressureValue treadDepth:(float) treadDepthValue{
    NSLog(@"子 initWithPressure:treadDepth");
    if(self = [super initWithPressure:pressureValue treadDepth:treadDepthValue]){
        rainHandling = 23.7;
        snowHandling = 42.5;
    }
    return self;
}

-(id)initWithRH:(float)rainValue sH:(float)snowValue
       pressure:(float) pressureValue treadDepth:(float) treadDepthValue{
    NSLog(@"子 initWithRH:sH:pressure:treadDepth");
    if(self = [super initWithPressure:pressureValue treadDepth:treadDepthValue]){
        rainHandling = rainValue;
        snowHandling = snowValue;
    }
    return self;

}
-(float)rainHandling{
    return rainHandling;
}
-(void)setRainHandling:(float)rainValue{
    rainHandling = rainValue;
}

-(float)snowHandling{
    return snowHandling;
}
-(void)setSnowHandling:(float)snowValue{
    snowHandling = snowValue;
}

-(NSString *)description{
    NSString * desc;
    desc = [[NSString alloc] initWithFormat:@"AllWeatherRadial: %.1f / %.1f / %.1f / %.1f", [self pressure], [self treadDepth], [self rainHandling], [self snowHandling]];
    return [desc autorelease];
}
@end
