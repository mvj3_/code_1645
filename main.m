//
//  main.m
//  Learning0508
//
//  Created by  apple on 13-5-8.
//  Copyright (c) 2013年 dhy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AllWeatherRadial.h"

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        NSMutableArray *tires = [NSMutableArray arrayWithCapacity:4];
        for(int i = 0 ; i < 4 ; i ++ ){
            AllWeatherRadial * tire;
            tire = [[AllWeatherRadial alloc] initWithPressure:33.3];
            [tires addObject:tire];
        }
        
        for(NSString *string in tires){
            NSLog(@"%@", string);
        }
        
    }
    return 0;
}

